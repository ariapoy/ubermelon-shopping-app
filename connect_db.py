import sqlite3
import pandas as pd

import pdb

# config
database = 'melons.db'

# main function
conn = sqlite3.connect(database)
melons = pd.read_sql("select * from melons", con=conn)
orders = pd.read_sql("select * from orders", con=conn)
order_items = pd.read_sql('select * from order_items', con=conn)
conn.close()

pdb.set_trace()
